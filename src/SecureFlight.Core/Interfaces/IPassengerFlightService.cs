﻿using System.Threading.Tasks;
using SecureFlight.Core.Entities;

namespace SecureFlight.Core.Interfaces
{
    public interface IPassengerFlightService
    {
        Task<OperationResult<PassengerFlight>> AddPassengerToFlight(long flightId, string passengerId);
    }
}
