﻿using System;
using System.Linq;
using System.Threading.Tasks;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services
{
    public class PassengerFlightService : BaseService<PassengerFlight>, IPassengerFlightService
    {
        private IRepository<Flight> _flightRepository;
        private IRepository<Passenger> _passengerRepository;


        public PassengerFlightService(IRepository<PassengerFlight> repository, IRepository<Flight> flightRepository,
            IRepository<Passenger> passengerRepository) : base(repository)
        {
            _flightRepository = flightRepository;
            _passengerRepository = passengerRepository;
        }

        private async Task<OperationResult> ValidatePassengerFlightAssignment(long flightId, string passengerId)
        {
            var errorMessage = String.Empty;
            if (string.IsNullOrWhiteSpace(passengerId))
                errorMessage = $"{passengerId} is required.";

            var passenger = await _passengerRepository.FilterAsync(p => p.Id == passengerId);
            if (!passenger.Any())
                errorMessage = $"No passenger found with id {passengerId}";

            var flight = await _flightRepository.FilterAsync(f => f.Id == flightId);
            if (!flight.Any())
                errorMessage = $"No flight found with id {flightId}";

            var passengerFlights =
                await _passengerRepository.FilterAsync(p =>
                    p.Id == passengerId && p.Flights.Any(x => x.Id == flightId));

            if (passengerFlights.Any())
                errorMessage = $"Passenger {passengerId} is already assigned to flight {flightId}";

            if (!string.IsNullOrEmpty(errorMessage))
                return new OperationResult(new Error() {Message = errorMessage});

            return new OperationResult(true);
        }

        public async Task<OperationResult<PassengerFlight>> AddPassengerToFlight(long flightId, string passengerId)
        {
            var validationResult = await ValidatePassengerFlightAssignment(flightId, passengerId);
            if (!validationResult.Succeeded)
                return new OperationResult<PassengerFlight>(validationResult.Error);

            var passengerFlight = new PassengerFlight()
            {
                FlightId = flightId,
                PassengerId = passengerId
            };

            _repository.Insert(passengerFlight);
            return new OperationResult<PassengerFlight>(true, passengerFlight);
        }
    }
}
